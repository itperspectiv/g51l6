//
//  FishPresentationVC.swift
//  G51L6
//
//  Created by Ivan Vasilevich on 2/9/17.
//  Copyright © 2017 Smoosh Labs. All rights reserved.
//

import UIKit

class FishPresentationVC: UIViewController {
    
    var buttonName = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = "Super Fish"
        
        let addFishButton = UIBarButtonItem(image: UIImage(named: "like"), style: .plain, target: self, action: #selector(addFishPressed))
        navigationItem.rightBarButtonItem = addFishButton
    }
    
    func addFishPressed() {
        print(#function)
    }

}
