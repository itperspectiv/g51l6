//
//  ViewController.swift
//  G51L6
//
//  Created by Ivan Vasilevich on 2/9/17.
//  Copyright © 2017 Smoosh Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //ViewControllerLiferCycle
    @IBOutlet weak var cornerSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        print(#function)
        print(cornerSwitch ?? "nil here")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print(#function)
        print(cornerSwitch)
//        view.backgroundColor = .lightGray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(#function)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        print(#function)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print(#function)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(#function)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print(#function)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print(#function)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print(#function)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(#function)
        if let titleButton = sender as? String {
            if let fisPresVC = segue.destination as? FishPresentationVC {
                fisPresVC.buttonName = titleButton
            }
            
        }
    }

    @IBAction func buttonPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "showFish", sender: sender.currentTitle)
    }
    
    
    
    @IBAction func switchChanged(_ sender: UISwitch) {
        LocalDB.sharedInstance.str1 = sender.isOn.description
    }
}

