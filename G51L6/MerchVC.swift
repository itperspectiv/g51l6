//
//  MerchVC.swift
//  G51L6
//
//  Created by Ivan Vasilevich on 2/9/17.
//  Copyright © 2017 Smoosh Labs. All rights reserved.
//

import UIKit

class MerchVC: UIViewController {

    @IBOutlet weak var switchStateLable: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        switchStateLable.text = LocalDB.sharedInstance.str1
    }

   

}
